/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var console = {
    log: function(what) {
        document.getElementById("log").innerHTML += what + "<br>";
    }
};

var app = {
    settings: {},
    noSound: 0,
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.themeSong = app.playSound("theme/whiskey");
        document.getElementById("loadingText").innerHTML = "Tap To Continue";
        app.beginTimeout = setTimeout(app.begin, 13000);
        //console.log("Device Ready");
        //document.getElementById("logo").setAttribute('style', 'display:none;');
        //app.receivedEvent('deviceready');

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        // console.log('Received Event: ' + id);
    },
    tappedBegin: function() {
        app.themeSong.stop();
        app.themeSong.release();
        clearTimeout(app.beginTimeout);
        app.begin();
    },
    begin: function() {
        document.getElementById("logo").setAttribute('style', 'display:none;');
        document.getElementById("main").setAttribute('style', 'display:block;');

        var tmpRes = app.getMaxResolution() 
        app.ringerToggleState = 0;
        //console.log(tmpRes[0] + "x" + tmpRes[1]);
    },
    beginNoSound: function() {
        document.getElementById("logo").setAttribute('style', 'display:none;');
        document.getElementById("main").setAttribute('style', 'display:block;');

        var tmpRes = app.getMaxResolution() 
        app.ringerToggleState = 0;
        app.noSound = 1;
        console.log("No Sound mode enabled");
    },
    playSound: function(which) {
        // console.log("playSound entered");
        if (app.ringerToggleState == 1) {
            var tmpFileName = "/android_asset/www/sounds/" + which + ".mp3";
            // console.log("Would set ringtone " + tmpFileName);
            app.exportAs = "ringtone";
            app.exportWhich = tmpFileName;
            app.exportShortName = which;
            app.showSetAsDialog();
        } else {
            if (app.noSound != 1) {
                var tmpFileName = "/android_asset/www/sounds/" + which + ".mp3";
                var theSound = new Media(tmpFileName,
                        function () {theSound.release();},
                        function (err) {}
                    );
                theSound.play();
                return theSound;
            } else {
                console.log("Would play sound.");
            }
        }
	},
    showSetAsDialog: function() {
        if (app.noSound == 1){console.log("Dialog Shown.")}
        document.getElementById("setAsDialog").setAttribute('style', 'display:block;');
        document.getElementById("setringer").setAttribute('style', 'display:none;');
    },
    cancelSetAsDialog: function() {
        if (app.noSound == 1){console.log("Dialog Hidden.")}
        document.getElementById("setAsDialog").setAttribute('style', 'display:none;');
        document.getElementById("setringer").setAttribute('style', 'display:block;');
        // app.toggleRinger();
    },
    setSoundAs: function(what) {
        // what = what || app.exportAs;
        if (app.noSound != 1) {
            var title = "FoF Sound " + app.exportShortName;
            window.ringtone.exportAssetAndSetRingtone(app.exportWhich, 
                    title, 
                    "Fistful of Frags Sound", 
                    what, 
                    function (success) {window.plugins.toast.showShortCenter(what + " set!");}, 
                    function (err) {window.plugins.toast.showShortCenter("error setting " + what +"!");console.log(err);});
            document.getElementById("setAsDialog").setAttribute('style', 'display:none;');
            document.getElementById("setringer").setAttribute('style', 'display:block;');
        } else {
            console.log("Would set " + what + " as file " + app.exportWhich + " with name " + app.exportShortName);
        }
    },
    getMaxResolution: function() {
        var w = [];
        w[0] = screen.availWidth || 999999999;
        w[1] = window.innerWidth || 999999999;
        w[2] = document.body.clientWidth || 999999999;
        w.sort(function(a, b) {
            return a - b;
        });

        var h = [];
        h[0] = screen.availHeight || 999999999;
        h[1] = window.innerHeight || 999999999;
        h[2] = document.body.clientHeight || 999999999;
        h.sort(function(a, b) {
            return a - b;
        });
        return [w[0],h[0]];
    },
    storeSettings: function(what) {
        localStorage.setItem("fistfulofsettings", JSON.stringify(app.settings));
    },
    restoreSettings: function() {
        app.settings = JSON.parse(localStorage.getItem("fistfulofsettings"));
    },
    toggleRinger: function() {
        if (app.ringerToggleState == 0) {
            // console.log("ringer set mode enabled.");
            app.playSound('desp/objective_letsgetgold');
            var tmp = document.getElementById("ringer-image");
            tmp.src = "./img/ringer32gold.png";

            var divs = document.getElementsByTagName("div");
            for(var i = 0; i < divs.length; i++){
                if (divs[i].className == "category-red") {
                    divs[i].className = "category-red-ringer";
                } else if(divs[i].className == "category-yellow") {
                    divs[i].className = "category-yellow-ringer";
                } else if(divs[i].className == "category-pasda") {
                    divs[i].className = "category-pasda-ringer";
                } else if(divs[i].className == "setringer") {
                    divs[i].className = "setringer-gold";
                } else if(divs[i].className == "category-blue") {
                    divs[i].className = "category-blue-ringer";
                } else if(divs[i].className == "category-green") {
                    divs[i].className = "category-green-ringer";
                }
            }
            app.ringerToggleState = 1;   
        } else {
            app.ringerToggleState = 0;
            // console.log("ringer set mode disabled.");
            app.playSound('desp/myloot_thatsmyloot');
            var tmp = document.getElementById("ringer-image");
            tmp.src = "./img/ringer32white.png";

            var divs = document.getElementsByTagName("div");
            for(var i = 0; i < divs.length; i++){
                if (divs[i].className == "category-red-ringer") {
                    divs[i].className = "category-red";
                } else if(divs[i].className == "category-yellow-ringer") {
                    divs[i].className = "category-yellow";
                } else if(divs[i].className == "category-pasda-ringer") {
                    divs[i].className = "category-pasda";
                } else if(divs[i].className == "setringer-gold") {
                    divs[i].className = "setringer";
                } else if(divs[i].className == "category-blue-ringer") {
                    divs[i].className = "category-blue";
                } else if(divs[i].className == "category-green-ringer") {
                    divs[i].className = "category-green";
                }
            }
        }
    }
};
app.initialize();