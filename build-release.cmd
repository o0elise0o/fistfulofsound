@echo off
echo -=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
cordova build android --release
pause
echo -=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
copy C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-unsigned.apk C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-signed.apk
echo -=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore fistfulofsound.keystore C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-signed.apk fistfulofsoundkey
echo -=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
jarsigner -verify -verbose -certs C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-signed.apk
echo -=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
C:\Users\elise\AppData\Local\Android\sdk\build-tools\22.0.1\zipalign.exe -f -v 4 C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-signed.apk C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-final.apk
echo -=-=-=--=-=-=--=-=-=--=-=-=--=-=-=-
copy C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\android-release-final.apk C:\Users\elise\Documents\cordova-projects\fistfulofsound\platforms\android\build\outputs\apk\fistfulofsound.apk